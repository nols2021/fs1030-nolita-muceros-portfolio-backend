import express from "express"; 
import validateData from "../functions.js"; // import function
import verifyToken from "../../src/middleware/jwtVerify.js";
import argon2 from "argon2";
import myConnection from "../../connection.js";


const userRouter = express.Router();

// checks invalid properties/data
const validateUserInput = (req, res, next) => {
    const usersValidProp = ["firstName", "lastName", "password", "email"]; // required properties
    let errorMessages = validateData("users", usersValidProp, req.body);
    if (errorMessages.length < 1) {
        next();
    } else {
        const errorMessageObj = {
            message: "validation error",
            invalid: errorMessages,
        };
        return res.status(400).json(errorMessageObj); // return error in json format indicating invalid/missing properties/data
    };  
};


/* 
* Private function for the module to hash the password
* @param password - password to hash
* @return hashed password
*/
const createHash = async (password) => {
    try {      
      const hash = await argon2.hash(password); // more rounds, means slower, but harder to figure out the hash
      return hash;
    } catch (err) {
      console.error(err);
    }
  }


/* 
* Private function for the module to verify the password
* @param hashedPW - hashed password
* @param password - entered password
* @return boolean
*/
const verifyPassword = async (hashedPW, password) => {
    try {
      let match = await argon2.verify(hashedPW, password);   
      return match;
    } catch (err) {
      console.error(err);
    }
  }


// View users
userRouter.get("/api/users", (req, res) => {
  myConnection.query("SELECT * FROM user", function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});


// Insert new user
userRouter.post("/api/users", validateUserInput, async (req, res, next) => {    
// userRouter.post("/api/users", verifyToken, validateUserInput, async (req, res, next) => {    
    try {
        const { firstName, lastName, password, email } = req.body;        
        const hashedPassword = await createHash(password); // hash password                               
        let newUserNoPW = {firstName, lastName, email };
        myConnection.query(
          "INSERT INTO user (user_first_name, user_last_name, password, email) VALUES (?, ?, ?, ?)",
          [req.body.firstName, req.body.lastName, hashedPassword, req.body.email],
          function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(newUserNoPW);
          }
        );
    } catch (err) {
        console.error("are we here?", err);
        return next(err);
    }
})


// Update a user
userRouter.put("/api/users/:id", verifyToken, validateUserInput, async (req, res) => {
  const { firstName, lastName, password, email } = req.body;
  const hashedPassword = await createHash(password);
  myConnection.query(
    `UPDATE user SET user_first_name="${firstName}", user_last_name="${lastName}", password="${hashedPassword}", email="${email}" WHERE user_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


// Delete a user
userRouter.delete("/api/users/:id", verifyToken, (req, res) => {
  myConnection.query(
    `DELETE FROM user WHERE user_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


export default userRouter;
import express from "express"; 
import verifyToken from "../../src/middleware/jwtVerify.js";
import argon2 from "argon2";
import myConnection from "../../connection.js";


const router = express.Router();

const validateEducation = (req, res, next) => {
  const { display_order, program, institution, period, resume_id } = req.body;
  if (resume_id === 1) {
      next();
  } else {
      const errorMessageObj = {
          message: "validation error",
          invalid: "The value of the Resume ID MUST BE 1!",
      };
      return res.status(400).json(errorMessageObj); 
  };  
};


// View education
router.get("/", (req, res) => {
  myConnection.query("SELECT * FROM education ORDER BY display_order", function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});


// Insert new education
router.post("/", verifyToken, validateEducation, (req, res, next) => {    
  try {
     const { display_order, program, institution, period, resume_id } = req.body; 
      // Check if entered display order already exists
      myConnection.query(
        `SELECT * FROM education WHERE display_order=${display_order}`,
        function (error, results, fields) {
          if (error) throw error;
          if(Object.keys(results).length > 0) {  
            return res.status(400).send('Sorry, the entered display order already exist!'); 
          } else {
            myConnection.query(
              "INSERT INTO education(display_order, program, institution, period, resume_id) VALUES (?, ?, ?, ?, ?)",
              [display_order, program, institution, period, resume_id],
              function (error, results, fields) {
                if (error) throw error;
                return res.status(201).send(results);
              }
            )    
          }  
        }
      )    
    } catch (err) {
      console.error("Error: ", err);
      return next(err);
    }
})


// Update education
router.put("/:id", verifyToken, async (req, res, next) => {    
  try {
     const { display_order, program, institution, period } = req.body; 
      // Check if entered display order already exists
      myConnection.query(
        `SELECT * FROM education WHERE display_order=${display_order}`,
        function (error, results, fields) {
          if (error) throw error;
          if(Object.keys(results).length > 0) {  
            return res.status(400).send('Sorry, the entered display order already exist!'); 
          } else {
            myConnection.query(
              `UPDATE education SET display_order="${display_order}", program="${program}", institution="${institution}", period="${period}" WHERE education_id=${req.params.id}`,
              function (error, results, fields) {
                if (error) throw error;
                return res.status(201).send(results);
              }
            )    
          }  
        }
      )    
    } catch (err) {
      console.error("Error: ", err);
      return next(err);
    }
})


// Delete education
router.delete("/:id", verifyToken, async (req, res) => {
  myConnection.query(
    `DELETE FROM education WHERE education_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


export default router;
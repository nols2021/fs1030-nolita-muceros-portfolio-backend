import express from "express"; 
import verifyToken from "../../src/middleware/jwtVerify.js";
import argon2 from "argon2";
import myConnection from "../../connection.js";


const router = express.Router();

const validateHighlights = (req, res, next) => {
  const { display_order, highlight, resume_id } = req.body; 
  if (resume_id === 1) {
      next();
  } else {
      const errorMessageObj = {
          message: "validation error",
          invalid: "The value of the Resume ID MUST BE 1!",
      };
      return res.status(400).json(errorMessageObj); 
  };  
};


// View all highlights
router.get("/", async (req, res) => {
      myConnection.query(`SELECT * FROM highlight ORDER BY display_order`, function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});


// View one highlight
router.get("/:id", async (req, res) => {
  myConnection.query(`SELECT * FROM highlight WHERE highlight_id=${req.params.id}`, function (error, results, fields) {
if (error) throw error;
return res.status(200).send(results);
});
});

// Insert highlight
// router.post("/", verifyToken, validateHighlights, async (req, res, next) => {    
router.post("/", verifyToken, validateHighlights, (req, res, next) => {  
  try {
      const { display_order, highlight, resume_id } = req.body;  
      // Check if entered display order already exists
      myConnection.query(
        `SELECT * FROM highlight WHERE display_order=${display_order}`,
        function (error, results, fields) {
          if (error) throw error;
          if(Object.keys(results).length > 0) {  
            return res.status(400).send('Sorry, the entered display order already exist!'); 
          } else {
            myConnection.query(
              "INSERT INTO highlight(display_order, highlight, resume_id) VALUES (?, ?, ?)",
              [display_order, highlight, resume_id],
              function (error, results, fields) {
                if (error) throw error;
                return res.status(201).send(results);
              }
            )    
          }  
        }
      )    
    } catch (err) {
      console.error("Error: ", err);
      return next(err);
    }
})


// Update highlight item
router.put("/:id", verifyToken, async (req, res, next) => {    
  try {
      const { display_order, highlight } = req.body;  
      // Check if entered display order already exists
      myConnection.query(
        `SELECT * FROM highlight WHERE display_order=${display_order}`,
        function (error, results, fields) {
          if (error) throw error;
          if(Object.keys(results).length > 0) {  
            return res.status(400).send('Sorry, the entered display order already exist!'); 
          } else {
            const sqlUpdate = `UPDATE highlight 
            SET display_order="${display_order}", 
            highlight="${highlight}"
            WHERE highlight_id=${req.params.id}`            
            myConnection.query(
              sqlUpdate, 
              function (error, results, fields) {
                if (error) throw error;
                return res.status(201).send(results);
              }
            )    
          }  
        }
      )    
    } catch (err) {
      console.error("Error: ", err);
      return next(err);
    }
})


// Delete highlight item
router.delete("/:id", verifyToken, (req, res) => {
  //Check if has highlight details (exists in highlight_detail table)
  myConnection.query(
    `SELECT * FROM highlight_detail WHERE highlight_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      if(Object.keys(results).length > 0) {        
        return res.status(400).send('Sorry, this highlight cannot be deleted because it exists in the highlight_detail table!'); 
      } else {
          myConnection.query(
            `DELETE FROM highlight WHERE highlight_id=${req.params.id}`,
            function (error, results, fields) {
              if (error) throw error;
              return res.status(200).send(results);
            }
          );
      }
    }
  );
});


export default router;
import express from "express"; 
import verifyToken from "../../src/middleware/jwtVerify.js";
import argon2 from "argon2";
import myConnection from "../../connection.js";


const router = express.Router();


const validateExperience= (req, res, next) => {
  const { display_order, company, period, job_title, resume_id } = req.body;
  if (resume_id === 1) {
      next();
  } else {
      const errorMessageObj = {
          message: "validation error",
          invalid: "The value of the Resume ID MUST BE 1!",
      };
      return res.status(400).json(errorMessageObj); 
  };  
};


// View experience
router.get("/", async (req, res) => {
  myConnection.query("SELECT * FROM experience ORDER BY display_order", function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});



// Insert experience
router.post("/", verifyToken, validateExperience, async (req, res, next) => {    
  try {
    const { display_order, company, period, job_title, resume_id } = req.body;
  
      // Check if entered display order already exists
      myConnection.query(
        `SELECT * FROM experience WHERE display_order=${display_order}`,
        function (error, results, fields) {
          if (error) throw error;
          if(Object.keys(results).length > 0) {  
            return res.status(400).send('Sorry, the entered display order already exist!'); 
          } else {
            myConnection.query(
              "INSERT INTO experience(display_order, company, period, job_title, resume_id) VALUES (?, ?, ?, ?, ?)",
              [display_order, company, period, job_title, resume_id],
              function (error, results, fields) {
                if (error) throw error;
                return res.status(201).send(results);
              }
            )    
          }  
        }
      )    
    } catch (err) {
      console.error("Error: ", err);
      return next(err);
    }
})


// Update experience
router.put("/:id", verifyToken, async (req, res, next) => {  
   try {
    const { display_order, company, period, job_title } = req.body; 
      // Check if entered display order already exists
      myConnection.query(
        `SELECT * FROM experience WHERE display_order=${display_order}`,
        function (error, results, fields) {
          if (error) throw error;
          if(Object.keys(results).length > 0) {  
            return res.status(400).send('Sorry, the entered display order already exist!'); 
          } else {
            const sqlUpdate = `UPDATE experience SET display_order="${display_order}", company="${company}", period="${period}", job_title="${job_title}" WHERE experience_id=${req.params.id}`            
            myConnection.query(
              sqlUpdate, 
              function (error, results, fields) {
                if (error) throw error;
                return res.status(201).send(results);
              }
            )    
          }  
        }
      )    
    } catch (err) {
      console.error("Error: ", err);
      return next(err);
    }
})

// Delete experience
router.delete("/:id", verifyToken, async (req, res) => {
  //Check if has work description (exists in work_description table)
  myConnection.query(
    `SELECT * FROM work_description WHERE experience_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      if(Object.keys(results).length > 0) {        
        return res.status(400).send('Sorry, this experience cannot be deleted because it exist in the work_description table!'); 
      } else {
          myConnection.query(
            `DELETE FROM experience WHERE experience_id=${req.params.id}`,
            function (error, results, fields) {
              if (error) throw error;
              return res.status(200).send(results);
            }
          );
      }
    }
  );
});

export default router;
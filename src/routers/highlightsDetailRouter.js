import express from "express"; 
import verifyToken from "../middleware/jwtVerify.js";
import argon2 from "argon2";
import myConnection from "../../connection.js";


const router = express.Router();


// View all highlights detail
router.get("/", async (req, res) => {
  myConnection.query(`SELECT * FROM highlight_detail ORDER BY highlight_id, display_order`, function (error, results, fields) {
  if (error) throw error;
    return res.status(200).send(results);
  });
});


// Insert new highligh detail 
router.post("/", verifyToken, (req, res, next) => {      
  try {
    const { display_order, detail, highlight_id } = req.body; 
 
      // Check if entered display order of the entered highlight already exists
      myConnection.query(
        `SELECT * FROM highlight_detail WHERE highlight_id = ${highlight_id} AND display_order=${display_order}`,
        function (error, results, fields) {
          if (error) throw error;
          if(Object.keys(results).length > 0) {  
            return res.status(400).send('Sorry, the entered display order of the entered highlight already exist!'); 
          } else {
                    // Check if entered highlight_id exists in the highlight table
                    myConnection.query(
                      `SELECT * FROM highlight WHERE highlight_id=${highlight_id}`,
                      function (error, results, fields) {
                        if (error) throw error;
                        if(Object.keys(results).length > 0) {           
                            myConnection.query("INSERT INTO highlight_detail(display_order, detail, highlight_id) VALUES (?, ?, ?)",
                            [display_order, detail, highlight_id],
                            function (error, results, fields) {
                              if (error) throw error;
                              return res.status(201).send(results);
                            }
                          ) 
                        } else {
                          return res.status(400).send('The highlights ID MUST exist in the highlight table!'); 

                        }
                      }
                    )    
                  }
        }      
      )    
    } catch (err) {
      console.error("Error: ", err);
      return next(err);
    }
})


// Update highligh detail
router.put("/:id", verifyToken, (req, res, next) => {    
  try {
    const { display_order, detail, highlight_id } = req.body;
 
      // Check if entered display order of the entered highlight already exists
      myConnection.query(
        `SELECT * FROM highlight_detail WHERE highlight_id = ${highlight_id} AND display_order=${display_order}`,
        function (error, results, fields) {
          if (error) throw error;
          if(Object.keys(results).length > 0) {  
            return res.status(400).send('Sorry, the entered display order of the entered highlight already exist!'); 
          } else {
                    // Check if entered highlight_id exists in the highlight table
                    myConnection.query(
                      `SELECT * FROM highlight WHERE highlight_id=${highlight_id}`,
                      function (error, results, fields) {
                        if (error) throw error;
                        if(Object.keys(results).length > 0) {  
                          const sqlUpdate = `UPDATE highlight_detail SET display_order="${display_order}", detail="${detail}", highlight_id="${highlight_id}" WHERE highlight_detail_id=${req.params.id}`            
                          myConnection.query(
                            sqlUpdate, 
                            function (error, results, fields) {
                              if (error) throw error;
                              return res.status(201).send(results);
                            }
                          ) 
                        } else {
                          return res.status(400).send('The highlights ID MUST exist in the highlight table!'); 

                        }
                      }
                    )    
                  }
        }      
      )    
    } catch (err) {
      console.error("Error: ", err);
      return next(err);
    }
})


// // Delete highlight detail
router.delete("/:id", verifyToken, (req, res) => {
  myConnection.query(
    `DELETE FROM highlight_detail WHERE highlight_detail_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});



export default router;
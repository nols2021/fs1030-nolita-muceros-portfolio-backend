import express from "express"; 
import verifyToken from "../../src/middleware/jwtVerify.js";
import argon2 from "argon2";
import myConnection from "../../connection.js";


const router = express.Router();

// View resume
router.get("/:id", (req, res) => {
    myConnection.query(`SELECT * FROM resume WHERE resume_id=${req.params.id}`, function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});


// Update resume
router.put("/:id", verifyToken, async (req, res) => {
  const { 
    first_name,
    middle_name,
    last_name,
    objective, 
    phone, 
    location, 
    email
  } = req.body;
  const sqlUpdate = `UPDATE resume 
  SET first_name="${first_name}",
  middle_name="${middle_name}",
  last_name="${last_name}",
  objective="${objective}", 
  phone="${phone}", 
  location="${location}", 
  email="${email}" 
  WHERE resume_id=${req.params.id}`

  myConnection.query(sqlUpdate,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router;

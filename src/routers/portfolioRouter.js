import express from "express"; 
import verifyToken from "../../src/middleware/jwtVerify.js";
import argon2 from "argon2";
import myConnection from "../../connection.js";


const router = express.Router();

const validatePortfolio = (req, res, next) => {
    const { display_order, title, sub_title, details, link, image_path, resume_id } = req.body;  
    if (resume_id === 1) {
        next();
    } else {
        const errorMessageObj = {
            message: "validation error",
            invalid: "The value of the Resume ID MUST BE 1!",
        };
        return res.status(400).json(errorMessageObj); 
    };  
  };


// View portfolio
router.get("/", (req, res) => {
    myConnection.query(`SELECT * FROM portfolio ORDER BY display_order`, function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});


// Insert new portfolio
router.post("/", verifyToken, validatePortfolio, (req, res, next) => {    
    try {
        const { display_order, title, sub_title, details, link, image_path, resume_id } = req.body;   
        // Check if entered display order already exists
        myConnection.query(
          `SELECT * FROM portfolio WHERE display_order=${display_order}`,
          function (error, results, fields) {
            if (error) throw error;
            if(Object.keys(results).length > 0) {  
              return res.status(400).send('Sorry, the entered display order already exist!'); 
            } else {
              myConnection.query(
                "INSERT INTO portfolio(display_order, title, sub_title, details, link, image_path, resume_id) VALUES (?, ?, ?, ?, ?, ?, ?)",
                [display_order, title, sub_title, details, link, image_path, resume_id],
                function (error, results, fields) {
                  if (error) throw error;
                  return res.status(201).send(results);
                }
              )    
            }  
          }
        )    
      } catch (err) {
        console.error("Error: ", err);
        return next(err);
      }
  })



// // Update portfolio
router.put("/:id", verifyToken, async (req, res, next) => {    
    try {
        const { display_order, title, sub_title, details, link, image_path, resume_id } = req.body; 
        // Check if entered display order already exists
        myConnection.query(
          `SELECT * FROM portfolio WHERE display_order=${display_order}`,
          function (error, results, fields) {
            if (error) throw error;
            if(Object.keys(results).length > 0) {  
              return res.status(400).send('Sorry, the entered display order already exist!'); 
            } else {
              const sqlUpdate = `UPDATE portfolio 
              SET display_order="${display_order}", 
              title="${title}", 
              sub_title="${sub_title}", 
              details="${details}", 
              link="${link}",
              image_path="${image_path}",
              resume_id="${resume_id}"
              WHERE portfolio_id=${req.params.id}`            
              myConnection.query(
                sqlUpdate, 
                function (error, results, fields) {
                  if (error) throw error;
                  return res.status(201).send(results);
                }
              )    
            }  
          }
        )    
      } catch (err) {
        console.error("Error: ", err);
        return next(err);
      }
  })


// // Delete portfolio
router.delete("/:id", verifyToken, async (req, res) => {
  myConnection.query(
    `DELETE FROM portfolio WHERE portfolio_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


export default router;

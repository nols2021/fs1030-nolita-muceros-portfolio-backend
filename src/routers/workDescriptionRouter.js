import express from "express"; 
import verifyToken from "../../src/middleware/jwtVerify.js";
import argon2 from "argon2";
import myConnection from "../../connection.js";


const router = express.Router();


// View all work descriptions
router.get("/", (req, res) => {
  myConnection.query("SELECT * FROM work_description ORDER BY experience_id, display_order", function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});


// Check if experience has related work description
router.get("/:id", (req, res) => {
  myConnection.query(`SELECT * FROM work_description WHERE experience_id=${req.params.id} ORDER BY experience_id, display_order`, function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});




// Insert new work description 
router.post("/", verifyToken, async (req, res, next) => {      
  try {
    const { experience_id, display_order, work_desc} = req.body; 
 
      // Check if entered display order of the entered work experience already exists
      myConnection.query(
        `SELECT * FROM work_description WHERE experience_id=${experience_id} AND display_order=${display_order}`,
        function (error, results, fields) {
          if (error) throw error;
          if(Object.keys(results).length > 0) {  
            return res.status(400).send('Sorry, the entered display order of the entered work experience already exist!'); 
          } else {
                    // Check if entered experience_id exists in the experience table
                    myConnection.query(
                      `SELECT * FROM experience WHERE experience_id=${experience_id}`,
                      function (error, results, fields) {
                        if (error) throw error;
                        if(Object.keys(results).length > 0) {           
                            myConnection.query("INSERT INTO work_description(experience_id, display_order, work_desc) VALUES (?, ?, ?)",
                            [experience_id, display_order, work_desc],
                            function (error, results, fields) {
                              if (error) throw error;
                              return res.status(201).send(results);
                            }
                          ) 
                        } else {
                          return res.status(400).send('The experience ID MUST exist in the experience table!'); 

                        }
                      }
                    )    
                  }
        }      
      )    
    } catch (err) {
      console.error("Error: ", err);
      return next(err);
    }
})


// Update work description 
router.put("/:id", verifyToken, async (req, res, next) => {      
  try {
    const { display_order, work_desc, experience_id } = req.body; 
 
      // Check if entered display order of the entered work experience already exists
      myConnection.query(
        `SELECT * FROM work_description WHERE experience_id=${experience_id} AND display_order=${display_order}`,
        function (error, results, fields) {
          if (error) throw error;
          if(Object.keys(results).length > 0) {  
            return res.status(400).send('Sorry, the entered display order of the entered work experience already exist!'); 
          } else {
                    // Check if entered experience_id exists in the experience table
                    myConnection.query(
                      `SELECT * FROM experience WHERE experience_id=${experience_id}`,
                      function (error, results, fields) {
                        if (error) throw error;
                        if(Object.keys(results).length > 0) {           
                            myConnection.query(`UPDATE work_description SET display_order="${display_order}", work_desc="${work_desc}", experience_id="${experience_id}" WHERE work_desc_id=${req.params.id}`,
                            function (error, results, fields) {
                              if (error) throw error;
                              return res.status(201).send(results);
                            }
                          ) 
                        } else {
                          return res.status(400).send('The experience ID MUST exist in the experience table!'); 

                        }
                      }
                    )    
                  }
        }      
      )    
    } catch (err) {
      console.error("Error: ", err);
      return next(err);
    }
})


// Delete work description
router.delete("/:id", verifyToken, (req, res) => {
  myConnection.query(
    `DELETE FROM work_description WHERE work_desc_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


export default router;
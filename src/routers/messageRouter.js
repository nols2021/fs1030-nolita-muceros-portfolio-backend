import express from "express"; 
import validateData from "../functions.js"; // import function
import verifyToken from "../../src/middleware/jwtVerify.js";
import argon2 from "argon2";
import myConnection from "../../connection.js";


const messageRouter = express.Router();

// checks invalid properties/data
const validateUserInput = (req, res, next) => {
    const usersValidProp = ["sender_fname, sender_lname, email, phone_number, message"]; // required properties
    let errorMessages = validateData("message", usersValidProp, req.body);
    if (errorMessages.length < 1) {
        next();
    } else {
        const errorMessageObj = {
            message: "validation error",
            invalid: errorMessages,
        };
        return res.status(400).json(errorMessageObj); // return error in json format indicating invalid/missing properties/data
    };  
};



// View message
messageRouter.get("/api/message", (req, res) => {
  myConnection.query("SELECT * FROM message", function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});


// Update message item
messageRouter.put("/api/message/:id", async (req, res) => {
  const { sender_fname, sender_lname, email, phone_number, message } = req.body;

  myConnection.query(
    `UPDATE message SET sender_fname="${sender_fname}", sender_lname="${sender_lname}", email="${email}", phone_number="${phone_number}", message="${message}", WHERE message_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


// Insert new message
messageRouter.post("/api/message", verifyToken, validateUserInput, async (req, res, next) => {    
    try {
        const { sender_fname, sender_lname, email, phone_number, message } = req.body;

        myConnection.query(
          "INSERT INTO message(sender_fname, sender_lname, email, phone_number, message) VALUES (?, ?, ?, ?, ?)",
          [sender_fname, sender_lname, email, phone_number, message],
          function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(newUserNoPW);
          }
        );
    } catch (err) {
        console.error("are we here?", err);
        return next(err);
    }
})





// Delete message item
messageRouter.delete("/api/message/:id", (req, res) => {
  myConnection.query(
    `DELETE FROM message WHERE message_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


export default messageRouter;
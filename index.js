import express from "express"; 
import cors from "cors";
import dotenv from 'dotenv';
dotenv.config();

import errorHandler from "./src/middleware/errorHandler.js"; // middleware to handle all other errors
import contactRouter from "./src/routers/contactRouter.js";
import userRouter from "./src/routers/userRouter.js";
import authRouter from "./src/routers/authRouter.js";
import resumeRouter from "./src/routers/resumeRouter.js";
import highlightsRouter from "./src/routers/highlightsRouter.js";
import highlightsDetailRouter from "./src/routers/highlightsDetailRouter.js";
import experienceRouter from "./src/routers/experienceRouter.js";
import workDescriptionRouter from "./src/routers/workDescriptionRouter.js";
import educationRouter from "./src/routers/educationRouter.js";
import portfolioRouter from "./src/routers/portfolioRouter.js";

// import "./connection.js";

// import jwt from "jsonwebtoken";

// import { createConnection } from "mysql";

const PORT = process.env.PORT || 8080;

const app = express(); 



// const port = process.env.PORT; // port required for this project

app.use(express.json()); // Express JSON parsing middleware
app.use(cors());



// app.use(function(req, res, next){
//     global.connection = mysql.createConnection({
//         host: "localhost",
//         user: "root",
//         password: "changed123",
//         database : 'mywebsite'
//     });
//     connection.connect(function (err) {
//         if (err) {
//           console.error("error connecting: " + err.stack);
//           return;
//         }
      
//         console.log("Database connected");
//       });
//     next();
// });



// mount route
app.use(userRouter);
app.use('/api/resume', resumeRouter);
app.use('/api/highlights', highlightsRouter);
app.use('/api/highlightsDetails', highlightsDetailRouter);
app.use('/api/experience', experienceRouter);
app.use('/api/workDescription', workDescriptionRouter);
app.use('/api/education', educationRouter);
app.use('/api/portfolio', portfolioRouter);


app.use(contactRouter);
app.use(authRouter);


// app.use('/api/users', userRouter)
// app.use('/api/auth', authRouter)
// app.use('/contact_form/entries', entryRoutes)
// app.use('/profile', profile)
// app.use('/careers', careers)
// app.use('/projects', projects)




// app.use(jwt({secret: process.env.JWT_SECRETKEY, algorithms: ['HS256']}));

app.use(errorHandler);

app.listen(PORT || 8080, () => {
        console.log(`API server is running on port ${PORT}`);
});
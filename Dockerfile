# FROM node:12.16.3-slim
FROM node:16.14

WORKDIR /app

COPY ./package*.json ./

RUN npm install

COPY ./ ./

RUN chmod +x wait-for-it.sh

CMD npm run start
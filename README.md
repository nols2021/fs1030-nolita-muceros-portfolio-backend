# Project: Personal Portfolio Website

This project utilized a containerized NodeJS application and Cloud MySQL for the API service while React for the frontend. 

The Docker image of the backend application was built locally based from the Dockerfile which then deployed onto the Cloud using the Google Cloud Registry service.

 The React application has been setup and deployed to the GCP Cloud as well.
 

## Technologies/packages/services used
    
    NodeJS / Express / Middleware / RESTful JSON API / Nodemon
    UUID / JWT Token / dotenv / Argon2 / React / Reactstrap / Cors


## To visit the deployed application

** IMPORTANT **
The API backend does not have table migration package and would need the tables/recods to be created in Cloud SQL. Please see "To Create the tables/records" section below first.

Visit the Domain name below for the deployed application. 
You will need to enter the credentials below to acess the website.

    Domain Name: myportfolio-website-gcp.nolita-muceros2.me
    
    Username:  adminuser@yehey.ca

    Password:   password


## To Create the tables/records

Please run the below SQL statements using the database tool of your choice in order to create the tables and records in the Cloud MySQL database used for this application.

        Cloud MySQL instance database pre-setup user:
            name: myuser
            password: mypassword


************** SQL STATEMENTS ***************

USE mywebsite;

CREATE TABLE IF NOT EXISTS user (
    user_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    user_first_name VARCHAR(40) NOT NULL,
    user_last_name VARCHAR(80) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(80) NOT NULL,
    PRIMARY KEY (user_id)
);

INSERT INTO user(user_first_name, user_last_name, password, email)
VALUES
("admin", "user", "$argon2i$v=19$m=4096,t=3,p=1$c9WDPK6i4Va55ySDpzy+ZQ$NsEW2PRRoQpQAeBVRjBCifBndEnLv4jHDJHt9AJKQDw", "adminuser@yehey.ca"),
("admin2", "user2", "$argon2i$v=19$m=4096,t=3,p=1$sotBHeLLeuDxFn4YMKAeIg$DW37iMQ/3q4zQVcsL2S/++rX7dHtRmikZigYpNVP2wY", "sample@user.com"),
("User1", "One", "$argon2i$v=19$m=4096,t=3,p=1$8Ajg3zvjku14NnGdur+8NQ$bLt6kmmf2S9W5IXTTRdm79mS2XC8EvfnwXqSskhm+FU", "user1@gmail.com"),
("User2", "Two", "$argon2i$v=19$m=4096,t=3,p=1$1v7pXPM4+NQxLzFwoLQaJQ$rSEBdx2YXc2FW0sp3AiDWhwvEPWRV67LEd2XBBL1Xqk", "user2@yehey.ca"),
("User3", "Three", "$argon2i$v=19$m=4096,t=3,p=1$zA+jPvLQO63rimgbgJvNjg$3/xGtJXZBBi4d35ui8lnMJNjt6x4llsumn4XxhNBbVI", "user3@yehey.com"),
("User4", "Four", "$argon2i$v=19$m=4096,t=3,p=1$FdsPbyR6excqxleuyLrtkA$wFIxl97gyuQiqHECzffwWWVZfRAVeYvUTYlXs8aoUcA", "user4@hotmail.com"),
("User5", "Five", "$argon2i$v=19$m=4096,t=3,p=1$GYeI+IKq8MfOf6dzy12YKg$ZAl7Dfepvtkoejw38fhFmaRoZRUWiolzXtlK2cvtyxo", "user5@bell.com");

CREATE TABLE IF NOT EXISTS resume (
    resume_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    objective VARCHAR(255) NOT NULL,
    phone VARCHAR(50) NOT NULL,
    location VARCHAR(255) NOT NULL,
    first_name VARCHAR(40) NOT NULL,
    middle_name VARCHAR(40) NOT NULL,
    last_name VARCHAR(80) NOT NULL,
    email VARCHAR(80) NOT NULL,
    PRIMARY KEY (resume_id)
);

INSERT INTO resume(objective, phone, location, first_name, middle_name, last_name, email)
VALUES
("Self-motivated and team player with years of experience as Developer seeking a position as a Web Developer where I can apply my enhanced knowledge in Web Design and Development to meet client standards and specifications", "6476743709", "Tottenhan, On L0G1W0", "Nolita", "", "Muceros", "nolitsmuceros@yahoo.com");



CREATE TABLE IF NOT EXISTS highlight (
    highlight_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    display_order INT UNSIGNED UNIQUE,
    highlight VARCHAR(1000) NOT NULL,
    resume_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (highlight_id),
    FOREIGN KEY (resume_id) REFERENCES resume(resume_id)
);

INSERT INTO highlight(display_order, highlight, resume_id)
VALUES 
(1, "Currently expanding skill set:", 1),
(2, "Few years experience as Developer", 1),
(3, "With experience in Automated Testing (UFT) / UI / Regression / QA TEsting", 1),
(4, "Few years experience as Developer", 1);



CREATE TABLE IF NOT EXISTS highlight_detail (
    highlight_detail_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    display_order INT UNSIGNED NOT NULL,
    detail VARCHAR(255) NOT NULL,
    highlight_id INT UNSIGNED NOT NULL,
    CONSTRAINT PK_HighLightDet PRIMARY KEY (highlight_detail_id, display_order),
    FOREIGN KEY (highlight_id) REFERENCES highlight (highlight_id)
);

INSERT INTO highlight_detail(display_order, detail, highlight_id)
VALUES
(1, "Front-end: HTML5, CSS3, Bootstrap, JavaScript (ES6+), JSX, HTML, DOM, React, AJAX", 1),
(2, "Back-end: Node.js, Express.js, REST, Restful Routes", 1),
(3, "Database: MySQL, mySQL Workbench, PHP, MongoDB", 1),
(4, "Project Management Tools: Trello, Gitlab, Github, Darw.io", 1),
(5, "Other: Promises, JASON, DOM API, Restful API, Fetch API,  Docker, GCP (Google Cloud Platform), AWS (Amazon Web Services), Git", 1);


CREATE TABLE IF NOT EXISTS education (
    education_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    display_order INT UNSIGNED NOT NULL UNIQUE,
    program VARCHAR(80) NOT NULL,
    institution VARCHAR(80) NOT NULL,
    period VARCHAR(50) NOT NULL,
    resume_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (education_id),
    FOREIGN KEY (resume_id) REFERENCES resume (resume_id)
);

INSERT INTO education(display_order, program, institution, period, resume_id)
VALUES
(1, "Full-Stack Web Development", "York University", "On-Going", 1),
(2, "Diploma in Programming - Visual Basic", "Professional Carrer Development Institute", "2002-2003", 1),
(3, "Master of Science in Computer Science", "AMA Computer Institute", "1998 to 1998", 1),
(4, "Bachelor of Science in Business Administration Major in Computer Science", "Central Colleges Of the Philippines", "1992 to 1996", 1);


CREATE TABLE IF NOT EXISTS experience (
    experience_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    display_order INT UNSIGNED NOT NULL UNIQUE,
    company VARCHAR(80) NOT NULL,
    period VARCHAR(50) NOT NULL,
    job_title VARCHAR(50) NOT NULL,
    resume_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (experience_id),
    FOREIGN KEY (resume_id) REFERENCES resume (resume_id)
);

INSERT INTO experience(display_order, company, period, Job_title, resume_id)
VALUES 
(1, "Honda Canada", "2012 - Present", "IT Analyst / QA Analyst", 1),
(2, "Honda Financial Services, Inc.", "2010 - 2012", "Systems Specialist", 1),
(3, "Honda Financial Services, Inc.", "2003 - 2010", "Credit Administrator / Credit Coordinator", 1),
(4, "Imperial Oil / SVE, Inc.", "2001 - 2002", "Office Support / Quality Clerk", 1),
(5, "Delco Wire and Cable Corporation", "2000 - 2001", "Computer Programmer", 1),
(6, "Nancy T. Ang and Associates", "1997 - 2000", "Computer Programmer", 1),
(7, "Shoemart, Inc.", "1996 - 1997", "Office Clerk", 1);

CREATE TABLE IF NOT EXISTS work_description (
    work_desc_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    display_order INT UNSIGNED NOT NULL,
    work_desc VARCHAR(255),
    experience_id INT UNSIGNED NOT NULL,
    CONSTRAINT PK_WorkDesc PRIMARY KEY (work_desc_id, display_order),
    FOREIGN KEY (experience_id) REFERENCES experience(experience_id)
);

INSERT INTO work_description(display_order, work_desc, experience_id)
VALUES
(1, "Manages systems access provisioning and ensure compliance with Sorbanes-Oxley Act (SOX) as pertains to the company's internal controls", 1),
(2, "Maintains excellent channels of communication with all customers (internal clients) and internal IT departments", 1),
(3, "Ensured all Service Level Agreements are maintained and issues/requests are resolved as per this commitment", 1),
(1, "Automated internal manual reports using VBScript", 2),
(2, "Developed/executed automated test scripts using HP UFT", 2),
(3, "Planned and developed Test Plans and Test Cases to ensure development outputs meet defined specificatio cases in coordination with Business requirements, Technical Specifications, installer / upgrades documents, and management", 2),
(4, "Performed Functional / Regression / SIT / Smoke / UI / QA / Web Browser Compatibility testings and analyzed results", 2),
(1, "Automated Loan/lease Assumption Contract using VBScript", 3),
(2, "Automated internal manual reports using VBScript", 3),
(3, "Processed Retail and Lease contracts, rebooks and balloon refinanciang in accordance of company's policies and procedures ", 3),
(4, "Maintained strong dealer relationships by dealing with their inquiries promptly and professionally", 3),
(1, "Automated internal manual reports using VBScript", 4),
(2, "Maintained Invoice MS Access database", 4),
(3, "Maintained and controlled company’s Documents Change Request File (manually and electronically)", 4),
(4, "Processed invoices via internal system", 4),
(1, "Enhanced and maintained Sales and Accounts Receivable System per business requirements", 5),
(2, "Analyzed and resolved issues relating to business Systems and ensured internal control is met", 5),
(3, "Prepared and maintained System Training Manual and Functional Specifications Manuals", 5),
(4, "Technologies: FoxPro 2.6 / Visual FoxPro 6.0 / DOS / NOVELL 3.12", 5),
(1, "Enhanced and maintained customized Sales and Accounts Receivable System / General Ledger and Budget Monitoring System / Inventory Control", 6),
(2, "Converted existing programs from Clipper to FoxPro", 6),
(3, "Provided technical support to system users", 6),
(4, "Technologies: Clipper 5.2/ dBase / FoxPro 2.6 / Visual FoxPro 6.0 / DOS / NOVELL 3.12 / WINDOWS NT 4.0", 6),
(1, "Performed clerical duties and responsibilitites related to consignment processing", 7);


CREATE TABLE IF NOT EXISTS portfolio (
    portfolio_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    display_order INT UNSIGNED NOT NULL UNIQUE,
    title VARCHAR(80) NOT NULL,
    sub_title VARCHAR(80) NOT NULL,
    details VARCHAR(3000) NOT NULL,
    link VARCHAR(255) NOT NULL,
    image_path VARCHAR(255) NOT NULL,
    resume_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (portfolio_id),
    FOREIGN KEY (resume_id) REFERENCES resume (resume_id)
);


INSERT INTO portfolio(display_order, title, sub_title, details, link, image_path, resume_id)
VALUES 
(1, "YOH", "YorkU Hackathon 2021 Winner!", "Winner of the York University Hackathon 2021. YOH! is a hub where tech graduates from York University are able to connect with recruiters for potential job opportunities. I worked mainly on the front-end.", "https://devpost.com/software/yoh-york-opportunity-hub", '/static/media/yoh.37c063f1.jpg', 1),
(2, "Photo Gallery", "Historical Sites of Toronto", "This Web Application is an independent work of mine showcasing the photo collection of the Historical Sites of City of Toronto. This comes with an overlay on hover of images as well as a custom styled zoom modal view upon clicking on a photo.", "https://github.com/nmuceros/FS1010_Assignment1_Style_Photo_Gallery", '/static/media/photoGallery.48fe261f.png', 1),
(3, "Todo App", "Interesting Drag-and-Drop Effect", "This Todo Web Application is an independent work of mine. Unlike normal Todo application, this has an interesting drag and drop effect.", "https://github.com/nmuceros/muceros__nolita--assignment-2", '/static/media/todoApp.afeaf5f0.png', 1),
(4, "Creative Resonance", "Mood Enhancing Site", "This is a Web Application showcasing the Music Album photo collections from different genres. It has music which automatically plays while a selected genre is being viewed. It is a group effort from FS1000 Music Group. The Admin page is my major contribution to this project.", "https://gitlab.com/nols2021/fs1000_summer2021_group1_project", '/static/media/creativeResonance.eff4bdab.jpg', 1);



CREATE TABLE IF NOT EXISTS message (
    message_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    sender_fname VARCHAR(40) NOT NULL,
    sender_lname VARCHAR(80) NOT NULL,
    email VARCHAR(80) NOT NULL,
    phone_number VARCHAR(10) NOT NULL,
    message VARCHAR(255) NOT NULL,
    PRIMARY KEY (message_id)
);

INSERT INTO message(sender_fname, sender_lname, email, phone_number, message)
VALUES
("John", "Doe", "johndoe@yehey.ca", "4161111111", "Hey! If you are still looking for work, please contact me."),
("Karen", "Carpentry", "karencarpentry@yehey.ca", "4162222222", "We are sorry, unfortunately we are looking for more experince programmer. Please keep an eye on our hiring site for futer opportunities"),
("Ringo", "Moon", "ringomoon@yehey.ca", "4163333333", "We are sorry but the position has already been taken."),
("Paul", "McCarth", "paulmaccarth@yehey.ca", "4164444444", "If you are still interested, please register on our website and someone will be get in touch with you."),
("George", "Horizon", "georgehorizon@yehey.ca", "4165555555", "Thank you for your inquiry. Someone will review your application and you will be contacted should you qualify for the position");

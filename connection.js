import { createConnection } from "mysql";
// import {} from 'dotenv/config'
// import dotenv from 'dotenv';
// dotenv.config();


// const connection = createConnection({
//   socketPath: process.env.DATABASE_SOCKET,
//   host: process.env.DATABASE_HOST,
//   user: process.env.DATABASE_USER,
//   password: process.env.DATABASE_PASSWORD,
//   database: process.env.DATABASE_NAME
// });


const configuration = {
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME
}

if (process.env.DATABASE_SOCKET) {
  configuration.socketPath = process.env.DATABASE_SOCKET
} else {
  configuration.host = process.env.DATABASE_HOST
}

const connection = createConnection(configuration);

// Docker-compose connection
// const connection = createConnection({
//   host: "db",
//   user: "myuser",
//   password: "mypassword",
//   database: "mywebsite"
// });

// Local Connection
// const connection = createConnection({
//   host: "localhost",
//   user: "root",
//   password: "changed123",
//   database: "mywebsite"
// });

// connection.connect(function (err) {
//   if (err) {
//     console.error(`error connecting: ${err.stack}`);
//     return;
//   }

//   console.log("Database connected");
  
// });

// connection.connect(function(err) {
//   if (err) throw err;
//   console.log("Connected!");
//   connection.query("CREATE DATABASE IF NOT EXISTS mywebsite", function (err, result) {
//     if (err) throw err;
//     console.log("Database created");
//     connection.query("USE mywebsite");
//   });
// });

export default connection;
